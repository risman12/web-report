import React, { useState } from "react";
import { Col, Row, Card, Table, Stack } from 'react-bootstrap';

import { Pagination, ButtonToolbar, IconButton } from 'rsuite';
import ExportIcon from '@rsuite/icons/Export';

const limitOptions = [30, 50, 100];

const Balance = () => {
    const [activePage, setActivePage] = React.useState(1);
    const [layout, setLayout] = React.useState(['total', '-', 'pager']);
    return (
        <>
            <Row>
                <Col md={12} xs={12}>
                    <Card className='p-3 rounded-0'>
                        <Table responsive bordered className="text-nowrap my-3">
                            <thead className="table-light">
                                <tr>
                                    <th>Waktu</th>
                                    <th>Jumlah</th>
                                    <th>Saldo Akhir</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </Table>

                        <Stack direction="horizontal" gap={3}>
                            <div>
                                <ButtonToolbar>
                                    <IconButton appearance="primary" color="green" icon={<ExportIcon />}>
                                        Export Excel
                                    </IconButton>
                                </ButtonToolbar>
                            </div>
                            <div className="ms-auto">
                                <Pagination
                                    layout={layout}
                                    size="sm"
                                    prev
                                    next
                                    first
                                    last
                                    ellipsis
                                    boundaryLinks
                                    total={30}
                                    limit={10}
                                    limitOptions={limitOptions}
                                    onChangePage={setActivePage}
                                />
                            </div>
                        </Stack>
                    </Card>
                </Col >
            </Row >
        </>
    )
}

export default Balance
