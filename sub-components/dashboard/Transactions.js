import React, { useState } from "react";
import { Col, Row, Card, Table, Form, Stack } from 'react-bootstrap';
import TransactionsData from "data/dashboard/TransactionsData";
import { DateRangePicker, Pagination, ButtonToolbar, IconButton } from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';
import ExportIcon from '@rsuite/icons/Export';

const limitOptions = [30, 50, 100];

const Transaction = () => {
    const [activePage, setActivePage] = React.useState(1);
    const [layout, setLayout] = React.useState(['total', '-', 'pager']);
    return (
        <>
            <Row>
                <Col md={12} xs={12}>
                    <Card className='p-3 rounded-0'>
                        <Form>
                            <Row className="align-items-center">
                                <Col xs="6">
                                    <Stack direction="horizontal" gap={3}>
                                        <DateRangePicker size="lg" placeholder="Select Date Range" />
                                        <Form.Group controlId={'input-2'}>
                                            <Form.Control
                                                name="input-2"
                                                placeholder="Tujuan"
                                                className="rounded-0"
                                            />
                                        </Form.Group>
                                        <div className="vr" />
                                        <ButtonToolbar>
                                            <IconButton size="md" appearance="primary" color="blue" icon={<SearchIcon />}>
                                                Cari
                                            </IconButton>
                                        </ButtonToolbar>
                                    </Stack>
                                </Col>
                            </Row>
                        </Form>
                        <Table responsive bordered className="text-nowrap my-3">
                            <thead className="table-light">
                                <tr>
                                    <th>Trx. ID</th>
                                    <th>Tgl. Entri</th>
                                    <th>Produk</th>
                                    <th>Tujuan</th>
                                    <th>Pengirim</th>
                                    <th>Status</th>
                                    <th>Tgl. Status</th>
                                    <th>Harga</th>
                                    <th>SN/Ref</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                {TransactionsData.map((item, index) => {
                                    return (
                                        <tr key={index}>
                                            <td className="align-middle">{item.trxId}</td>
                                            <td className="align-middle">{item.tglEntri}</td>
                                            <td className="align-middle">{item.produk}</td>
                                            <td className="align-middle">{item.tujuan}</td>
                                            <td className="align-middle">{item.pengirim}</td>
                                            <td className="align-middle">{item.status}</td>
                                            <td className="align-middle">{item.tglStatus}</td>
                                            <td className="align-middle">{item.harga}</td>
                                            <td className="align-middle">{item.sn}</td>
                                            <td className="align-middle">{item.keterangan}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </Table>

                        <Stack direction="horizontal" gap={3}>
                            <div>
                                <ButtonToolbar>
                                    <IconButton appearance="primary" color="green" icon={<ExportIcon />}>
                                        Export Excel
                                    </IconButton>
                                </ButtonToolbar>
                            </div>
                            <div className="ms-auto">
                                <Pagination
                                    layout={layout}
                                    size="sm"
                                    prev
                                    next
                                    first
                                    last
                                    ellipsis
                                    boundaryLinks
                                    total={30}
                                    limit={10}
                                    limitOptions={limitOptions}
                                    onChangePage={setActivePage}
                                />
                            </div>
                        </Stack>
                    </Card>
                </Col >
            </Row >
        </>
    )
}

export default Transaction
