import React, { useState } from "react";
import { Col, Row, Card, Table, Stack, Form } from 'react-bootstrap';

import { DateRangePicker, Pagination, ButtonToolbar, IconButton } from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';
import ExportIcon from '@rsuite/icons/Export';

const limitOptions = [30, 50, 100];

const TransactionDownline = () => {
    const [activePage, setActivePage] = React.useState(1);
    const [layout, setLayout] = React.useState(['total', '-', 'pager']);
    return (
        <>
            <Row>
                <Col md={12} xs={12}>
                    <Card className='p-3 rounded-0'>
                        <Form>
                            <Row className="align-items-center">
                                <Col xs="8">
                                    <Stack direction="horizontal" gap={3}>
                                        <DateRangePicker size="lg" placeholder="Select Date Range" />
                                        <Form.Group controlId={'input-2'}>
                                            <Form.Control
                                                name="input-2"
                                                placeholder="Tujuan"
                                                className="rounded-0"
                                            />
                                        </Form.Group>
                                        <Form.Group controlId={'input-2'}>
                                            <Form.Control
                                                name="input-2"
                                                placeholder="Kode Downline"
                                                className="rounded-0"
                                            />
                                        </Form.Group>
                                        <div className="vr" />
                                        <ButtonToolbar>
                                            <IconButton size="md" appearance="primary" color="blue" icon={<SearchIcon />}>
                                                Cari
                                            </IconButton>
                                        </ButtonToolbar>
                                    </Stack>
                                </Col>
                            </Row>
                        </Form>
                        <Table responsive bordered className="text-nowrap my-3">
                            <thead className="table-light">
                                <tr>
                                    <th>Trx. ID</th>
                                    <th>Tgl. Entri</th>
                                    <th>Produk</th>
                                    <th>Tujuan</th>
                                    <th>Downline</th>
                                    <th>Status Saldo</th>
                                    <th>Tgl. Status</th>
                                    <th>Harga</th>
                                    <th>SN/Ref</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </Table>

                        <Stack direction="horizontal" gap={3}>
                            <div>
                                <ButtonToolbar>
                                    <IconButton appearance="primary" color="green" icon={<ExportIcon />}>
                                        Export Excel
                                    </IconButton>
                                </ButtonToolbar>
                            </div>
                            <div className="ms-auto">
                                <Pagination
                                    layout={layout}
                                    size="sm"
                                    prev
                                    next
                                    first
                                    last
                                    ellipsis
                                    boundaryLinks
                                    total={30}
                                    limit={10}
                                    limitOptions={limitOptions}
                                    onChangePage={setActivePage}
                                />
                            </div>
                        </Stack>
                    </Card>
                </Col >
            </Row >
        </>
    )
}

export default TransactionDownline
