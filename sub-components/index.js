/**
 * The folder sub-components contains sub component of all the pages,
 * so here you will find folder names which are listed in root pages.
 */

import Transactions from 'sub-components/dashboard/Transactions';
import Balances from 'sub-components/dashboard/Balances';
import Prices from 'sub-components/dashboard/Prices';
import Downlines from 'sub-components/dashboard/Downlines';
import TransactionDownlines from 'sub-components/dashboard/TransactionDownlines';

export {
   Transactions,
   Balances,
   Prices,
   Downlines,
   TransactionDownlines,
};
