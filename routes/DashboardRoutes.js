import { v4 as uuid } from 'uuid';

export const DashboardMenu = [
	{
		id: uuid(),
		title: 'Lihat Transaksi',
		icon: 'clipboard',
		link: '/pages/transaction'
	},
	{
		id: uuid(),
		title: 'Mutasi Saldo',
		icon: 'briefcase',
		link: '/pages/balance'
	},
	{
		id: uuid(),
		title: 'Harga',
		icon: 'credit-card',
		link: '/pages/price'
	},
	{
		id: uuid(),
		title: 'Downline',
		icon: 'user',
		link: '/pages/downline'
	},
	{
		id: uuid(),
		title: 'Transaksi Downline',
		icon: 'info',
		link: '/pages/transaction-downline'
	},
];

export default DashboardMenu;
