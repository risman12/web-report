"use client";
// import node module libraries
import { Fragment, useContext } from "react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useMediaQuery } from "react-responsive";
import { ListGroup, Card, Image, Badge } from "react-bootstrap";
import Accordion from "react-bootstrap/Accordion";
import AccordionContext from "react-bootstrap/AccordionContext";
import { useAccordionButton } from "react-bootstrap/AccordionButton";

// import simple bar scrolling used for notification item scrolling
import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";

// import routes file
import { DashboardMenu } from "routes/DashboardRoutes";

const NavbarVertical = (props) => {
    const location = usePathname();

    return (
        <Fragment>
            <SimpleBar style={{ maxHeight: "100vh" }}>
                <div className="nav-scroller bg-primary">
                    <Link href="/" className="navbar-brand">
                        <h4 className="text-light text-center fw-bold mb-0">Web Report</h4>
                    </Link>
                </div>

                <div className="nav-scroller">
                    <div className="navbar-brand">
                        {/* <p className="mb-0">Saldo: Rp 2.000.000</p> */}
                    </div>
                </div>
                {/* Dashboard Menu */}
                <Accordion
                    defaultActiveKey="0"
                    as="ul"
                    className="navbar-nav flex-column"
                >
                    {DashboardMenu.map(function (menu, index) {
                        return (
                            <Card bsPrefix="nav-item" key={index}>
                                {/* menu item without any childern items like Documentation and Changelog items*/}
                                <Link
                                    href={menu.link}
                                    className={`nav-link ${location === menu.link ? "active" : ""
                                        } ${menu.title === "Download" ? "bg-primary text-white" : ""
                                        }`}
                                >
                                    {typeof menu.icon === "string" ? (
                                        <i className={`nav-icon fe fe-${menu.icon} me-2`}></i>
                                    ) : (
                                        menu.icon
                                    )}
                                    {menu.title}
                                    {menu.badge ? (
                                        <Badge
                                            className="ms-1"
                                            bg={menu.badgecolor ? menu.badgecolor : "primary"}
                                        >
                                            {menu.badge}
                                        </Badge>
                                    ) : (
                                        ""
                                    )}
                                </Link>
                                {/* end of menu item without any childern items */}
                            </Card>
                        );
                    })}
                </Accordion>
                {/* end of Dashboard Menu */}
            </SimpleBar>
        </Fragment>
    );
};

export default NavbarVertical;
