import { Fragment } from 'react';
import { useMediaQuery } from 'react-responsive';
import Link from 'next/link';
import {
    ListGroup,
    Button,
} from 'react-bootstrap';

import 'simplebar/dist/simplebar.min.css';
import useMounted from 'hooks/useMounted';

import { Dropdown, Avatar } from 'rsuite';
import UserIcon from '@rsuite/icons/legacy/User';

const renderToggle = props => (
    <Avatar circle {...props} appereance="primary">
        <UserIcon />
    </Avatar>
);

const QuickMenu = () => {

    const hasMounted = useMounted();
    const isDesktop = useMediaQuery({
        query: '(min-width: 1224px)'
    })

    const QuickMenuDesktop = () => {
        return (
            <ListGroup as="ul" bsPrefix='navbar-nav' className="navbar-right-wrap ms-auto d-flex nav-top-wrap">
                <Dropdown renderToggle={renderToggle} placement="bottomEnd">
                    <Dropdown.Item style={{ padding: 10, width: 160 }}>
                        <Link href="/authentication/sign-in" className="fs-5">
                            Sign Out
                        </Link>
                    </Dropdown.Item>
                </Dropdown>
            </ListGroup>
        )
    }

    const QuickMenuMobile = () => {
        return (
            <ListGroup as="ul" bsPrefix='navbar-nav' className="navbar-right-wrap ms-auto d-flex nav-top-wrap">
                <ListGroup as="ul" bsPrefix='navbar-nav' className="navbar-right-wrap ms-auto d-flex nav-top-wrap">
                    <Dropdown renderToggle={renderToggle} placement="bottomEnd">
                        <Dropdown.Item style={{ padding: 10, width: 160 }}>
                            <Link href="/authentication/sign-in" className="fs-5">
                                Sign Out
                            </Link>
                        </Dropdown.Item>
                    </Dropdown>
                </ListGroup>
            </ListGroup>
        )
    }

    return (
        <Fragment>
            {hasMounted && isDesktop ? <QuickMenuDesktop /> : <QuickMenuMobile />}
        </Fragment>
    )
}

export default QuickMenu;
