import 'styles/theme.scss';
import 'rsuite/dist/rsuite.min.css';

export const metadata = {
    title: 'Web Report - Sultan Gaming',
}

export default function RootLayout({ children }) {
    return (
        <html lang="en">
            <body className='bg-light'>
                {children}
            </body>
        </html>
    )
}
