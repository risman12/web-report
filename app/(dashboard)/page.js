'use client'
import { Fragment } from "react";
import Link from 'next/link';
import { Container, Col, Row } from 'react-bootstrap';

import {
    Transactions
} from "sub-components";

const Home = () => {
    return (
        <Fragment>
            <Container fluid className=" p-6">
                <Row>
                    <Col lg={12} md={12} sm={12}>
                        <div className="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
                            <div className="mb-3 mb-md-0">
                                <h1 className="mb-0 h2 fw-bold">Lihat Transaksi</h1>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Transactions />
            </Container>
        </Fragment>
    )
}
export default Home;
