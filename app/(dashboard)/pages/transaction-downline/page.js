'use client'

// import node module libraries
import { Col, Row, Container } from 'react-bootstrap';
import { TransactionDownlines } from 'sub-components';

const TransactionDownline = () => {
	return (
		<Container fluid className="p-6">
			<Row>
				<Col lg={12} md={12} sm={12}>
					<div className="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
						<div className="mb-3 mb-md-0">
							<h1 className="mb-0 h2 fw-bold">Transaksi Downline</h1>
						</div>
					</div>
				</Col>
			</Row>
			<TransactionDownlines />
		</Container>
	);
};

export default TransactionDownline;
